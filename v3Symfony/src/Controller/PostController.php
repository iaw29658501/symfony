<?php

namespace App\Controller;

use App\Entity\Post;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/post", name="post")
 */

class PostController extends Controller
{
    /**
     * @Route("/create", name="create")
     */
    public function create(): Response
    {
        return $this->json([
            'message' => 'Welcome to your new controller!',
            'path' => 'src/Controller/PostController.php',
        ]);
    }

    /**
     * @Route("/", name="index")
     */
    public function index(): Response
    {
        return $this->render('post/index.html.twig');
    }

    /**
     * @Route("/show/", name="show")
     * @return Response
     */
    public function show(): Response
    {
        //dd($post);
        $inipath = php_ini_loaded_file();
        if ($inipath) {
            return $this->json([
                'message' => 'Loadd php.ini: ' . $inipath,
                'path' => 'src/Controller/PostController.php',
            ]);
        } else {
            return $this->json([
                'message' => 'No foud php.ini: ' ,
            'path' => 'src/Controller/PostController.php',
            ]);
        }


    }

    /**
     * @Route("/edit/{id}", name="edit")
     */
    public function edit(): Response
    {
        return $this->json([
            'message' => 'Welcome to edit',
            'path' => 'src/Controller/PostController.php',
        ]);
    }

    /**
     * @Route("/delete/{id}", name="delete")
     * @param Post $post
     * @return Response
     */
    public function delete(Post $post): Response
    {
        $em = $this->getDoctrine()->getManager();
        $em->delete($post);
        $em->flush();
        return $this->json([
            'message' => 'Deleted, i guess...',
        ]);
    }





}
