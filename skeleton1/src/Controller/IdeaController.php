<?php

namespace App\Controller;

use App\Entity\Idea;
use App\Repository\IdeaRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
/**
 * @Route("/idea", name="idea")
 */
class IdeaController extends AbstractController
{
    /**
     * @Route("/index", name="index")
     * @param IdeaRepository $ideaRepository
     * @return Response
     */
    public function index(IdeaRepository $ideaRepository): Response
    {
       // $ideas = $ideaRepository->findAll();
        return $this->render("idea/index.html.twig", array(
            'ideas' => 'idea'
        ));
    }

    /**
     * @Route("/create", name="create")
     * @param Request $req
     * @return Response
     */
    public function create(Request $req ): Response
    {
        $idea = new Idea();
        $idea->setTitle('an app of ideas');
        $idea->setDescription('the app should be a crud for ideas, users and admins');
        // entity manager to save
        $em = $this->getDoctrine()->getManager();
        $em->persist($idea);
        $em->flush();
        return new Response('Created');
    }

}
