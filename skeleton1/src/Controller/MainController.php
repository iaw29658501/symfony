<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class MainController extends AbstractController
{

    /**
     * @Route("/",name="home")
     */
    public function home(): Response
    {
        return new Response('<h1>Home</h1>');
    }

    /**
     * @Route("/ideas/",name="ideas")
     * @param Request $request
     * @return Response
     */
    public function ideas(Request $request): Response
    {
        return $this->render('ideas/all.html.twig',
            [
                'idea'=> $request->get('idea')
            ]);
    }

    /**
     * @Route("/main", name="main")
     */
    public function index(): Response
    {
        return $this->json([
            'message' => 'Welcome to your new controller!',
            'path' => 'src/Controller/MainController.php',
        ]);
    }
}
