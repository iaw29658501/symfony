<?php

namespace App\DataFixtures;

use App\Entity\Idea;
use App\Entity\Tag;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{
    private $encoder;
    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder= $encoder;
    }

    public function load(ObjectManager $manager)
    {

        $tag_ti=new Tag();
        $tag_ti->setName('ti');
        $manager->persist($tag_ti);

        $tag_marketing=new Tag();
        $tag_marketing->setName('marketing');
        $manager->persist($tag_marketing);

        $tag_web=new Tag();
        $tag_web->setName('web');
        $manager->persist($tag_web);

        $tag_web=new Tag();
        $tag_web->setName('ingeniería');
        $manager->persist($tag_web);

        $tag_adm=new Tag();
        $tag_adm->setName('administración');
        $manager->persist($tag_adm);

        $user = new User();
        $user->setUsername('admin');
        $user->setPassword($this->encoder->encodePassword($user,'admin123'));
        $user->setRoles(['ROLE_ADMIN']);
        $manager->persist($user);

        $user1 = new User();
        $user1->setUsername('cau');
        $user1->setPassword($this->encoder->encodePassword($user1,'cau'));
        $user1->setRoles(['ROLE_USER']);
        $user1->setIsThemeSocial(true);
        $idea = new Idea();
        $idea->setTitle('Lorem solem golem ipsum');
        $idea->setDescription("La capacitosium detectorious poderious essere molto pius avanzamentum per nosostramus come un grandiosium qualitus bicicletarious na movimentum l'seguretar tutom");
        $idea->addTag($tag_adm);
        $idea->setUser($user1);
        $manager->persist($idea);
        $manager->persist($user1);


        $manager->flush();
    }
}
