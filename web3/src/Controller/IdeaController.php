<?php

namespace App\Controller;

use App\Entity\Comment;
use App\Entity\Idea;
use App\Entity\Tag;
use App\Form\CommentType;
use App\Form\IdeaType;
use App\Repository\IdeaRepository;
use App\Repository\LikeRepository;
use App\Repository\TagRepository;
use DateTime;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;
use App\Entity\Like;
use App\Entity\User;

/**
 * @Route("/idea")
 */
class IdeaController extends Controller
{
    /**
     * @Route("/", name="idea_index", methods={"GET"})
     */
    public function index(IdeaRepository $ideaRepository): Response
    {
       $ideas = $ideaRepository->findAll();
        return $this->showIdeas($ideas);
    }

    private function showIdeas($ideas): ?Response
    {
        $comment_forms = [];
        $user = $this->getUser();
        if ( $user->getIsThemeSocial()) {
            foreach ($ideas as $idea) {
                $comment = new Comment();
                $comment->setIdea($idea);
                $comment_form = $this->createForm(CommentType::class, $comment);
                $comment_forms[$idea->getId()] = $comment_form->createView();
            }
            $template = 'idea/index.html.twig';
        } else {
            $template = 'idea/table.html.twig';
        }
        return $this->render($template, [
            'ideas' => $ideas,
            'user' => $user,
            'comment_forms' => $comment_forms
        ]);
    }

    /**
     * @Route("/new", name="idea_new", methods={"GET","POST"})
     * @param Request $request
     * @param UserInterface $user
     * @return Response
     */
    public function new(Request $request): Response
    {
        $idea = new Idea();
        $form = $this->createForm(IdeaType::class, $idea);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->save_image_in_form_if_any($form, $idea);
            $entityManager = $this->getDoctrine()->getManager();
            foreach($idea->getTags() as $tag) {
                $tag->addIdea($idea);
                $entityManager->persist($tag);
            }
            $idea->setPubDate(new DateTime());
            $idea->setUser($this->getUser());
            $entityManager->persist($idea);
            $entityManager->flush();
            $this->addFlash('success','Idea created - you are amazing!');
            return $this->redirectToRoute('idea_index');
        }

        return $this->render('idea/new.html.twig', [
            'idea' => $idea,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="idea_show", methods={"GET"})
     */
    public function show(Idea $idea): Response
    {
        $comment = new Comment();
        $comment_forms[$idea->getId()] = $this
            ->createForm(CommentType::class, $comment)
            ->createView();

        return $this->render('idea/show.html.twig', [
            'idea' => $idea,
            'comment_forms'=>$comment_forms
        ]);
    }

    /**
     * @Route("/{id}/edit", name="idea_edit", methods={"GET","POST"})
     * @param Request $request
     * @param Idea $idea
     * @return Response
     */
    public function edit(Request $request, Idea $idea): Response
    {
        $oldTags = clone $idea->getTags();

        if(! $this->getUser() || !$this->getUser()->hasAuth($idea)) {
            return $this->json([
                'error'=>'Not Authorized'
            ]);
        }
        $form = $this->createForm(IdeaType::class, $idea);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->save_image_in_form_if_any($form,$idea);
            $newTags = clone $idea->getTags();
            $entityManager = $this->getDoctrine()->getManager();
            foreach ($oldTags as $oldTag) {
                $oldTag->removeIdea($idea);
                $entityManager->persist($oldTag);
            }
            foreach($newTags as $newTag) {
                $newTag->addIdea($idea);
                $entityManager->persist($newTag);
            }
            $this->getDoctrine()->getManager()->flush();
            return $this->redirectToRoute('idea_index');
        }

        return $this->render('idea/edit.html.twig', [
            'idea' => $idea,
            'form' => $form->createView(),
        ]);
    }
    /**
     * @Route("/{id}", name="idea_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Idea $idea): Response
    {
        if(! $this->getUser()->hasAuth($idea) ) {
            return $this->json([
                'error'=>'Not Authorized'
            ]);
        }
        if ($this->isCsrfTokenValid('delete'.$idea->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($idea);
            $entityManager->flush();
        }

        return $this->redirectToRoute('idea_index');
    }

    /**
     * @Route("/{id}/change_like", name="idea_change_like", methods={"GET"})
     */
    public function change_like(Request $request, Idea $idea, LikeRepository $likeRepository, UserInterface $user): Response
    {
        // check if this user already liked this idea or not
        $em = $this->getDoctrine()->getManager();
        $qb = $em->createQueryBuilder()
        ->select('l')
        ->from(Like::class,'l')
        ->where('l.idea = :idea')
        ->andWhere('l.user = :user')
        ->setParameter('idea', $idea->getId())
        ->setParameter('user', $user->getId());
        if ($this->isCsrfTokenValid($this->getUser()->getId(), $request->get('_token'))) {
            // if already liked dislike it
            $likes = $qb->getQuery()->getResult();
            $count = count($likes);
            if ($count) {
                //$entityManager = $this->getDoctrine()->getManager();
                $em->remove($likes[0]);
                $em->flush();
            } else {
                //if not liked, change to like it
                $em = $this->getDoctrine()->getManager();
                $like = (new Like())
                    ->setUser($user)
                    ->setIdea($idea);
                $em->persist($like);
                $em->flush();
            }
            return $this->json([
                'count' => $idea->getLikes()->count()
            ]);
        } else {
            $this->addFlash("error","Invalid request");
        }
        return $this->redirectToRoute('idea_index');
    }

    /**
     * @Route("/{id}/change_approve", name="idea_change_approve", methods={"POST"})
     * @Security("is_granted('ROLE_ADMIN')")
     */
    public function change_approve(Request $request, Idea $idea, LikeRepository $likeRepository, UserInterface $user): Response
    {
        // check if this user already liked this idea or not
        if ($this->isCsrfTokenValid($this->getUser()->getId(), $request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            // change approved state
            $idea->setApproved(!$idea->getApproved());
            $em->persist($idea);
            $em->flush();
            return $this->json([
                'success' => true
            ]);
        }
        $this->addFlash("error","Invalid request");
        return $this->redirectToRoute('idea_index');
    }

    /**
     * @Route("/tag/{tag}", name="idea_by_tag", methods={"GET"})
     * @param IdeaRepository $ideaRepository
     * @param Tag $tag
     * @return Response
     */
    public function tag(Tag $tag): Response
    {
        $ideas = $tag->getIdea();
        return $this->showIdeas($ideas);
    }

    private function save_image_in_form_if_any(FormInterface $form, Idea $idea): void
    {
        // TODO: remove the old image
        /** @var UploadedFile $image */
        $imageFile = $form->get('image')->getData();
        // if image (because it is not required)
        if ($imageFile) {
            $originalFilename = pathinfo($imageFile->getClientOriginalName(), PATHINFO_FILENAME);
            // this is needed to safely include the file name as part of the URL
            $safeFilename = transliterator_transliterate('Any-Latin; Latin-ASCII; [^A-Za-z0-9_] remove; Lower()', $originalFilename);
            $newFilename = $safeFilename.'-'.uniqid('', true).'.'.$imageFile->guessExtension();

            // Move the file to the directory where image idea's are stored
            try {
                $imageFile->move(
                    $this->getParameter('idea_images_directory_local'),
                    $newFilename
                );
            } catch (FileException $e) {
                $this->addFlash('error','Sorry, I could not save the image :(');
            }
            // updates the 'imageFilename' property to store the PDF file name
            // instead of its contents
            $idea->setImage($newFilename);
        }
    }


}
