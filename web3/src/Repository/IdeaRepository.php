<?php

namespace App\Repository;

use App\Entity\Idea;
use App\Entity\Tag;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Idea|null find($id, $lockMode = null, $lockVersion = null)
 * @method Idea|null findOneBy(array $criteria, array $orderBy = null)
 * @method Idea[]    findAll()
 * @method Idea[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class IdeaRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Idea::class);
    }

    /**
     * @return Idea[]|array
     */
    public function findAll()
    {
        return $this->findBy(array(),array('id'=>'DESC'));
    }
     /**
      * @return Idea[] Returns an array of Idea objects
      */
    public function findByTag( Tag $tag)
    {
        return $this->createQueryBuilder('i')
         //  ->innerJoin(Tag::class,'t')
            ->where('(:tag) IN i.tags')
            ->setParameter('tag', $tag)
            ->getQuery()
            ->getResult();
//
//        return $this->createQueryBuilder('i')
//            ->andWhere('i.exampleField = :val')
//            ->setParameter('val', $tag)
//            ->orderBy('i.id', 'DESC')
//          //  ->setMaxResults(200)
//            ->getQuery()
//            ->getResult()
//        ;
    }

    /*
    public function findOneBySomeField($value): ?Idea
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
