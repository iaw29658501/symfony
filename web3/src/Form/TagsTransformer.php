<?php
namespace App\Form;

use App\Entity\Tag;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Form\DataTransformerInterface;

class TagsTransformer implements DataTransformerInterface
{

    /**
     * @var ObjectManager
     */
    private $entity_manager;


    // Get the data from the database and show to the user

    /**
     * TagsTransformer constructor.
     * @param ObjectManager $entity_manager
     */
    public function __construct(ObjectManager $entity_manager)
    {
        $this->entity_manager = $entity_manager;
    }

    public function transform($value)
    {
        return implode(', ',$value);
       // return "hello world";
    }
    // reverse of the one above, from user input to db
    public function reverseTransform($value)
    {
        $tag_names = explode(',',$value);
        // remove excessive spaces from each tag_name
        $tag_names = array_map('trim',$tag_names);
        // remove duplicates and empty ones... (they return false by filter)
        // ex: tag1,,tag2 ... the one in the middle is empty
        $tag_names = array_filter(array_unique($tag_names));
        // every one in lowercase
        $tag_names = array_map('strtolower',$tag_names);

        $tags_that_already_exist = $this->entity_manager
            ->getRepository(Tag::class)
            ->findBy([
                'name' => $tag_names
            ]);
        $tags_that_dont_exist_yet = array_diff($tag_names, $tags_that_already_exist);
        $tags = $tags_that_already_exist;
        foreach ($tags_that_dont_exist_yet as $tag_name) {
            $tag = new Tag();
            $tag->setName($tag_name);
            $tags[]=$tag;
        }
        return $tags;
    }
}