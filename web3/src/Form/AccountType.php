<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AccountType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username')
        //    ->add('roles')
        //    ->add('password')
            ->add('is_theme_social',ChoiceType::class,[
            'choices' => [
                'Social Media (Facebook)' => true,
                'Tables (EXCEL)' => false,
            ],
            'label'=>'Theme'
        ])
            ->add('accepted_confidentiality')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
