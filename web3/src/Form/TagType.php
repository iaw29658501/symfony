<?php

namespace App\Form;

use App\Entity\Tag;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Doctrine\Form\DataTransformer\CollectionToArrayTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TagType extends AbstractType
{

    /**
     * @var EntityManager
     */
    private $entity_manager;

    /**
     * TagType constructor.
     * @param EntityManagerInterface $entity_manager
     */
    public function __construct(EntityManagerInterface $entity_manager)
    {
        $this->entity_manager = $entity_manager;
    }


    public function getParent() {
        return TextType::class;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->addModelTransformer(new CollectionToArrayTransformer(),true)
        ->addModelTransformer(new TagsTransformer($this->entity_manager),true);
    }
//
//    public function buildForm(FormBuilderInterface $builder, array $options)
//    {
//        $builder
//            ->add('name')
//            //->add('idea')
//        ;
//    }
//
}
