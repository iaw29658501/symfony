/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.css in this case)
import './styles/app.css';

// Need jQuery? Install it with "yarn add jquery", then uncomment to import it.
import $ from 'jquery';

import 'fontawesome';

import 'bootstrap';

$(document).ready(function(){
    $('.sendChangeLikeRequest').on('click', function (e) {
        // prevent default behaviour of browser to follow links
        e.preventDefault()
        var next = $(this).next();
        var ideaId = $(this).data('ideaid');
        var token = $(this).data('token');
        $.get('/idea/'+ideaId+'/change_like', { _token: token}
        ).done(function (response) {
            $('#likes-counter-'+ideaId)[0].innerHTML=response['count'];
        }).fail(function (response) {
            console.log('error: like state not changed')
        });
    });

    $('.sendDeleteRequest').on('click', function () {
        var ideaId=$(this).data('ideaid');
        var token=$(this).data('token');
        $.post('/idea/'+ideaId,
            {
                _token: token,
                _method: 'DELETE',
            }
        ).done(function (response) {
            console.log('delete state changed');
            console.log(response)
            //location.reload();
        }).fail(function () {
            console.log('error: like state not changed')
        });
    });

    $('.sendDeleteCommentRequest').on('click', function () {
        var commentId=$(this).data('commentid');
        var token=$(this).data('token');
        $.post('/comment/'+commentId,
            {
                _token: token,
                _method: 'DELETE',
            }
        ).done(function (response) {
            console.log('deleted comment');
            console.log("lets remove " + commentId)
            $("#comment-"+commentId).remove()
        }).fail(function () {
            console.log('error: like state not changed')
        });
    });

    $("input:checkbox").change(function() {
        var ideaid=$(this).data('ideaid');
        var token = $(this).data('token');
        var url = "/idea/" + ideaid + "/change_approve"
        $.ajax({
                url: url,
                type: 'POST',
                data: { _token:token }
                }
        );
    });
});
